﻿using Arendelle.GMap.Wrapper.Data;
using System;

namespace MapTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Great Map Wrapper Test - The Upgrade to .NET Standard - August 2018");
            Console.WriteLine("By Chris Zenzel (ArendelleKingdom.com)");
            Console.WriteLine();

            var objWrapper = new Arendelle.GMap.Wrapper.Providers.Bing();

            var objCenterPt = new Arendelle.GMap.Wrapper.Data.Geopoint()
            {
                Latitude = 28.2950,
                Longitude = -81.6129
            };

            var aTiles = objWrapper.GetTiles(new Arendelle.GMap.Wrapper.Data.TileBounds()
            {
                Minimum = new Arendelle.GMap.Wrapper.Data.Geopoint()
                {
                    Latitude = (objCenterPt.Latitude - 3.0),
                    Longitude = (objCenterPt.Longitude - 3.0)
                },
                Maximum = new Arendelle.GMap.Wrapper.Data.Geopoint()
                {
                    Latitude = (objCenterPt.Latitude + 3.0),
                    Longitude = (objCenterPt.Longitude + 3.0)
                }
            });

            DrawMap(aTiles);

            Console.ReadLine();
        }

        static void DrawMap(Tile[] Tiles)
        {
        }
    }
}
