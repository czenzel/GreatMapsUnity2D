﻿using System;
using System.Collections.Generic;
using System.Text;
using Arendelle.GMap.Wrapper.Data;

namespace MapTest
{
    public static class TileExtensions
    {
        public static string DebugString(this Tile value)
        {
            var objStrBuilder = new StringBuilder();

            objStrBuilder.AppendLine(string.Format("Tile Data ({0} x {1})", value.Size.Height, value.Size.Width));
            objStrBuilder.AppendLine(string.Format("Regional Data ({0} x {1})", value.Region.Height, value.Region.Width));
            objStrBuilder.AppendLine(string.Format("Upper Left Graphics Data ({0}, {1})", value.UpperLeft.X, value.UpperLeft.Y));
            objStrBuilder.AppendLine(string.Format("Tile Data ({0} bytes)", value.Image.Data.Length));

            return objStrBuilder.ToString();
        }
    }
}
