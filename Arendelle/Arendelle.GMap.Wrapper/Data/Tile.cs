﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Arendelle.GMap.Wrapper.Data
{
    public class Tile
    {
        public Size Size { get; set; }
        public Point UpperLeft { get; set; }
        public Size Region { get; set; }
        public Image Image { get; set; }
    }
}
