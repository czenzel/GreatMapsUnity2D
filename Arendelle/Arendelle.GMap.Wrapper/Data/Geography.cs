﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Arendelle.GMap.Wrapper.Data
{
    public class TileBounds
    {
        public Geopoint Minimum { get; set; }
        public Geopoint Maximum { get; set; }
    }

    public class Geopoint
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
    }
}
