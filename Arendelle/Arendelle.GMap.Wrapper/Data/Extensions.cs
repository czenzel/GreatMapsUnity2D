﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Arendelle.GMap.Wrapper.Data
{
    public static class Extensions
    {
        public static Tile[] RebaseTileRegions(this Tile[] value)
        {
            var dRegionHeight = value.Select(a => a.Size.Height).AsEnumerable()
                .Sum();

            var dRegionWidth = value.Select(a => a.Size.Width).AsEnumerable()
                .Sum();

            foreach (var single in value)
            {
                for (var y = 0; y <= single.Size.Height; y++)
                {
                    for (var x = 0; x <= single.Size.Width; x++)
                    {
                        var dTileX = (dRegionWidth -
                            (single.UpperLeft.X + x));

                        var dTileY = (single.UpperLeft.Y +
                            single.Size.Height - y);

                        if (single.UpperLeft != null &&
                            !single.UpperLeft.TestPoint(dTileX, dTileY) &&
                            (dTileX >= 0 && dTileY >= 0) &&
                            (dTileX <= dRegionWidth && dTileY <= dRegionHeight))
                        {
                            single.UpperLeft.X = dTileX;
                            single.UpperLeft.Y = dTileY;
                        }

                        if (single.Region == null)
                            single.Region = new Size()
                            {
                                Width = dRegionWidth,
                                Height = dRegionHeight
                            };
                    }
                }
            }

            return value;
        }

        private static bool TestPoint(this Point value, double X, double Y)
        {
            return ((value.X == X) &&
                (value.Y == Y));
        }
    }
}
