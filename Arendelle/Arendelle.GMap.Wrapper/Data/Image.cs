﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Arendelle.GMap.Wrapper.Data
{
    public class Image
    {
        public byte[] Data { get; set; }

        public ImageFormatting Format { get; set; }
    }

    public enum ImageFormatting
    {
        Png = 0,
        Jpg = 1
    }
}
