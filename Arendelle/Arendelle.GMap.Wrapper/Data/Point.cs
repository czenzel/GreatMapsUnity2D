﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Arendelle.GMap.Wrapper.Data
{
    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }
    }
}
