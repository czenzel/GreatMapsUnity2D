﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Arendelle.GMap.Wrapper.Data
{
    public class Size
    {
        public double Width { get; set; }
        public double Height { get; set; }
    }
}
