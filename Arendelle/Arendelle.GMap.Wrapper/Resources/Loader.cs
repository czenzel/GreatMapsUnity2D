﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Arendelle.GMap.Wrapper.Resources
{
    internal class Loader
    {
        public static string StringFromFile(string Locator)
        {
            string strReturn = null;

            try
            {
                var strActualLocation = string.Format("Arendelle.GMap.Wrapper.{0}", Locator);
                var objAssembly = typeof(Loader).Assembly;

                var objResource = objAssembly.GetManifestResourceStream(strActualLocation);

                using (var objReader = new StreamReader(objResource))
                {
                    strReturn = objReader.ReadToEnd();
                    objReader.Close();
                }
            }
            catch
            {
                strReturn = null;
            }

            return strReturn;
        }
    }
}
