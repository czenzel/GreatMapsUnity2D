﻿using System;
using System.IO;

using System.Collections;
using System.Collections.Generic;

using GMap.NET;
using GMap.NET.MapProviders;

using System.Reflection;

using Arendelle.GMap.Wrapper.Data;

namespace Arendelle.GMap.Wrapper.Providers
{
    public class Bing
    {
        public Tile[] GetTiles(TileBounds bounds, int zoom = 8)
        {
            GMaps.Instance.UseMemoryCache = false;
            GMaps.Instance.Mode = AccessMode.ServerOnly;

            var objProvider = GMapProviders.BingMap;

            try
            {
                objProvider.TryGetDefaultKey = false;
                objProvider.ClientKey = Resources.Loader.StringFromFile("Resources.BingKey.txt");
            }
            catch { objProvider.TryGetDefaultKey = true; }

            objProvider.OnInitialized();

            var objRect = RectLatLng.FromLTRB(bounds.Minimum.Longitude, bounds.Maximum.Latitude,
                bounds.Maximum.Longitude, bounds.Minimum.Latitude);

            List<Tile> aTiles = new List<Tile>();

            if (!objRect.IsEmpty)
            {
                List<GPoint> lTileArea = objProvider.Projection.GetAreaTileList(objRect, zoom, 0);

                var objTopLeftPx = objProvider.Projection.FromLatLngToPixel(objRect.LocationTopLeft, zoom);
                var objRightBtmPx = objProvider.Projection.FromLatLngToPixel(objRect.LocationRightBottom, zoom);

                var objPxDelta = new GPoint(objRightBtmPx.X - objTopLeftPx.X,
                    objRightBtmPx.Y - objTopLeftPx.Y);

                // var iPadding = 22;

                /* not working - order of op (OOO) issues */
                /*
                var dXCalc = (objProvider.Projection.TileSize.Width - objTopLeftPx.X + iPadding);
                var dYCalc = (objProvider.Projection.TileSize.Height - objTopLeftPx.Y + iPadding);
                */

                /*
                 * Distance in Pixels attempt (8/22/2018):
                 * Width (X) = Upper Left Lon to Upper Right Lon
                 * Height (Y) = Upper Left Lat to Bottom Left Lat
                 */
                
                var dRegionWidth = objProvider.Projection.GetDistanceInPixels(
                    objProvider.Projection.FromLatLngToPixel(objRect.Top, objRect.Left, zoom),
                    objProvider.Projection.FromLatLngToPixel(objRect.Top, objRect.Right, zoom)
                );

                var dRegionHeight = objProvider.Projection.GetDistanceInPixels(
                    objProvider.Projection.FromLatLngToPixel(objRect.Top, objRect.Left, zoom),
                    objProvider.Projection.FromLatLngToPixel(objRect.Bottom, objRect.Left, zoom)
                );

                foreach (var objArea in lTileArea)
                {
                    foreach (var objOverlay in objProvider.Overlays)
                    {
                        var strTileUri = IntPtr_Uri(objProvider, objArea, zoom);

                        var objPoint = objArea;

                        var objTileCoords = new Point()
                        {
                            X = objPoint.X,
                            Y = objPoint.Y
                        };

                        objTileCoords.X = (int)(objTileCoords.X * objProvider.Projection.TileSize.Width - objTopLeftPx.X);
                        objTileCoords.Y = (int)(objTileCoords.Y * objProvider.Projection.TileSize.Height - objTopLeftPx.Y);

                        var objMapData = new Tile()
                        {
                            UpperLeft = objTileCoords,
                            Size = new Size()
                            {
                                Width = objProvider.Projection.TileSize.Width,
                                Height = objProvider.Projection.TileSize.Height
                            },
                            Region = new Size()
                            {
                                Width = dRegionWidth,
                                Height = dRegionHeight
                            },
                            Image = new Image()
                            {
                                Data = Internal_Download(strTileUri),
                                Format = ImageFormatting.Jpg
                            }
                        };

                        aTiles.Add(objMapData);
                    }
                }
            }

            var aOutputTiles = aTiles.ToArray();
            // possibly retire old Unity3D code? - aOutputTiles.RebaseTileRegions();

            return aOutputTiles;
        }

        private string Language { get; } = @"en";

        private string IntPtr_Uri(object Provider, GPoint pos, int zoom)
        {
            var objType = typeof(BingMapProvider);
            var objMethod = objType.GetMethod(@"MakeTileImageUrl",
                BindingFlags.NonPublic | BindingFlags.Instance);

            var objResult = objMethod.Invoke(Provider, new object[]
            {
                pos,
                zoom,
                Language
            });

            var strResult = Convert.ToString(objResult);
            return strResult;
        }

        private byte[] Internal_Download(string Uri)
        {
            var objWebClient = new System.Net.WebClient();
            var aBytes = objWebClient.DownloadData(Uri);
            return aBytes;
        }
    }
}
