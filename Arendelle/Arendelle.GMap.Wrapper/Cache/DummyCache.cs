﻿using System;
using System.Collections.Generic;
using System.Text;

using GMap.NET;
using GMap.NET.CacheProviders;

namespace Arendelle.GMap.Wrapper.Cache
{
    public class DummyCache : PureImageCache
    {
        public int DeleteOlderThan(DateTime date, int? type)
        {
            return 0;
        }

        public PureImage GetImageFromCache(int type, GPoint pos, int zoom)
        {
            return null;
        }

        public bool PutImageToCache(byte[] tile, int type, GPoint pos, int zoom)
        {
            return false;
        }
    }
}
